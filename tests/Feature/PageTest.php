<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PageTest extends TestCase
{
    use RefreshDatabase;

    public function testWelcomePage()
    {
        $this
            ->get('/')
            ->assertStatus(200)
            ->assertSeeTextInOrder([
                'e-kierowca',
                'Zadanie testowe',
                'Opis zadania',
            ]);
    }

    public function testAppPageShouldBeInaccessibleForAnonymousUsers()
    {
        $this
            ->get(route('app'))
            ->assertStatus(401);
    }

    public function testAppPageShouldBeAccessibleForLoggedInUsers()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $this
            ->actingAs($user)
            ->get(route('app'))
            ->assertStatus(200);
    }
}
